#!/bin/sh

#  Script.sh
#  
#
#  Created by asdf on 25.12.21.
#  


curl -X PUT "localhost:9200/wildfires?pretty" -H 'Content-Type: application/json' -d'
        {
        "settings": {
            "number_of_shards": 1
            },
            "mappings": {
                "properties": {
                    "objectid": {
                        "type": "long"
                        },
                    "fod_id": {
                        "type": "long"
                        },
                    "fpa_id": {
                        "type": "text"
                        },
                    "source_system": {
                        "type": "keyword"
                        },
                    "nwcg_reporting_unit_id": {
                        "type": "text"
                        },
                    "nwcg_reporting_unit_name": {
                        "type": "keyword"
                        },
                    "local_incident_id": {
                        "type": "text"
                        },
                    "fire_code": {
                        "type": "keyword"
                        },
                    "fire_name": {
                        "type": "text"
                        },
                    "fire_size_keyword": {
                        "type": "keyword"
                        },
                    "fire_size": {
                        "type": "double"
                        },
                    "stat_cause_descr": {
                        "type": "keyword"
                        },
                    "discovery_date": {
                        "type": "date"
                        },
                    "geographic_coordinates": {
                        "type": "geo_point"
                        }
                }
            }
        }'



node newjavascript.js


