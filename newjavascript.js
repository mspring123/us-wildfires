/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require("array.prototype.flatmap").shim();
const { Client } = require("@elastic/elasticsearch");

const client = new Client({
    node: "http://localhost:9200"
});


var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database("/Users/asdf/Downloads/FPA_FOD_20170508.sqlite");


//db.serialize(function() {
//    
//  db.each("SELECT OBJECTID AS id, FIRE_NAME FROM Fires", function(err, row) {
//      console.log(row.id + ": " + row.FIRE_NAME);
//  });
//});
//
//db.close();

var get = [ ];
var nor = 0;

function getCount () {
    return new Promise((resolve, reject) => {
        db.all("select count(*) from Fires", [ ], (err, rows) => {
            if ( err ) {
                return console.error(err.message)
            }
            get = rows;
            resolve(parseInt(get[0]["count(*)"]));
//            console.log(count[0]["count(*)"]);
        })
    })
}



var data = [ ],
        dataset = [ ];

function getRecords (a, b) {
    return new Promise((resolve, reject) => {
        db.all("select objectid,fod_id,fpa_id,source_system,nwcg_reporting_unit_id,nwcg_reporting_unit_name,\n\
                            local_incident_id,fire_code,fire_name,fire_size,stat_cause_descr,date(discovery_date)as discovery_date,\n\
                            longitude,latitude from fires where objectid between " + a + " and " + b, [ ], (err, rows) => {
            if ( err ) {
                return console.error(err.message);
            }
            rows.forEach((row) => {
                var doc = {
                    "objectid": parseInt(row["OBJECTID"]),
                    "fod_id": parseInt(row["FOD_ID"]),
                    "fpa_id": String(row["FPA_ID"]),
                    "source_system": String(row["SOURCE_SYSTEM"]),
                    "nwcg_reporting_unit_id": String(row["NWCG_REPORTING_UNIT_ID"]),
                    "nwcg_reporting_unit_name": String(row["NWCG_REPORTING_UNIT_NAME"]),
                    "local_incident_id": String(row["LOCAL_INCIDENT_ID"]),
                    "fire_code": String(row["FIRE_CODE"]),
                    "fire_name": String(row["FIRE_NAME"]),
                    "fire_size_keyword": String(row["FIRE_SIZE"]),
                    "fire_size": parseFloat(row["FIRE_SIZE"]),
                    "stat_cause_descr": String(row["STAT_CAUSE_DESCR"]),
                    "discovery_date": new Date(row["discovery_date"]),
                    "geographic_coordinates": {
                        "lat": row["LATITUDE"],
                        "lon": row["LONGITUDE"]
                    }

                }
                data.push(doc);
//                console.log(row['FOD_ID'])
            });

            resolve(data);
        });

    });
}

//(async function () {
//    records = await getRecords(12, 16);
//    console.log(records[0]);
//})();


(async function () {
    let start = Date.now();

    nor = await getCount();
    console.log(nor);

    for ( let i = 0; i <= nor; i ++ ) {
        dataset.length = 0;
        if ( i % 10000 === 0 ) {
            var a = (i - 9999);
            var b = i;
            console.log("a: " + parseInt(a));
            console.log("b: " + parseInt(b));

            dataset = await getRecords(a, b);

            console.log("dataset.length: " + parseInt(dataset.length));
            console.log(dataset[0]);

            if ( parseInt(dataset.length) != 0 ) {
                const body = dataset.flatMap(doc => [ { index: { _index: "wildfires" } }, doc ])

                const { body: bulkResponse } = await client.bulk({ refresh: true, body })

                if ( bulkResponse.errors ) {
                    const erroredDocuments = [ ]
                    // The items array has the same order of the dataset we just indexed.
                    // The presence of the `error` key indicates that the operation
                    // that we did for the document has failed.
                    bulkResponse.items.forEach((action, i) => {
                        const operation = Object.keys(action)[0]
                        if ( action[operation].error ) {
                            erroredDocuments.push({
                                // If the status is 429 it means that you can retry the document,
                                // otherwise it's very likely a mapping error, and you should
                                // fix the document before to try it again.
                                status: action[operation].status,
                                error: action[operation].error,
                                operation: body[i * 2],
                                document: body[i * 2 + 1]
                            })
                        }
                    })
                    console.log(erroredDocuments)
                }
            }

        }
    }
    const { body: count } = await client.count({ index: "wildfires" })
    console.log(count)

    let stop = Date.now();

    ts = stop - start;
    // timestamp in milliseconds
    console.log("timestamp in milliseconds: " + ts);

    // timestamp in seconds
    console.log(Math.floor("imestamp in seconds" + (ts / 1000)));
})();

