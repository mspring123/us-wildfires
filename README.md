# us-wildfires

## Project goal

The aim is to analyze the forest fires of the last decades.

#### Source reference

further information and descriptions can be found under the following kaggle source [Source reference](https://www.kaggle.com/rtatman/188-million-us-wildfires)


## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:ddf518e71423bf0122c08846f07f4d04?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:ddf518e71423bf0122c08846f07f4d04?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:ddf518e71423bf0122c08846f07f4d04?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mspring123/us-wildfires.git
git branch -M main
git push -uf origin main
```

> Author *`michaelspring123@web.de`*
> > Creation date *`2021-12-26`*



#  <#Title#>

# The project flow

1. **Download** the sqlite
    - [188-million-us-wildfires](https://www.kaggle.com/rtatman/188-million-us-wildfires)
2. Creating the **data pipeline**
    - Creating the *`node-js`* project
    - Transforming the relational structure into an object structure
3. **Analyze** and present the data


## Required libraries

### @elastic/elasticsearch

> asdf@asdfs-MBP us-wildfires % *`npm i @elastic/elasticsearch`*

### sqlite3

> asdf@asdfs-MBP us-wildfires % *`npm i sqlite3`*

### array.prototype.flatmap

> asdf@asdfs-MBP us-wildfires % *`npm i array.prototype.flatmap`*


# reading sqlite with node-js

## selection of the database

```javascript
var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database("/Downloads/FPA_FOD_20170508.sqlite");
```

## execute the test query

<details>
<summary>structure of test query</summary>
<p>

```javascript
var data = [ ],
        records = [ ];

function getRecords (a, b) {
    return new Promise((resolve, reject) => {
        db.all("select objectid,source_system from fires where objectid between " + a + " and " + b, [ ], (err, rows) => {
            if ( err ) {
                return console.error(err.message);
            }
            rows.forEach((row) => {
                var doc = {
                    "objectid": parseInt(row["OBJECTID"]),
                    "source_system": String(row["SOURCE_SYSTEM"])
                }
                data.push(doc);
            });
            resolve(data);
        });
    });
}
```
</p>
</details>


### call the async function


<details>
<summary>async function</summary>
<p>

```javascript
(async function () {
    records = await getRecords(12, 16);
    console.log(records);
})();
```
</p>
</details>

### the console output

<details>
<summary>the console output</summary>
<p>

```console
asdf@asdfs-MacBook-Pro us-wildfires % node newjavascript2.js
[
  { objectid: 12, source_system: 'FS-FIRESTAT' },
  { objectid: 13, source_system: 'FS-FIRESTAT' },
  { objectid: 14, source_system: 'FS-FIRESTAT' },
  { objectid: 15, source_system: 'FS-FIRESTAT' },
  { objectid: 16, source_system: 'FS-FIRESTAT' }
]
asdf@asdfs-MacBook-Pro us-wildfires % 
```
</p>
</details>






## Possible problems

### heap limit Allocation failed

It is essential to be careful when querying large amounts of data from a database. The data must or will be cached in variables. If the amount of data here is too large, an overflow occurs.


```console
asdf@asdfs-MacBook-Pro sqlite % node asdf.js                      

<--- Last few GCs --->

[9710:0x102aa2000]   124422 ms: Mark-sweep 2048.6 (2051.8) -> 2048.3 (2052.1) MB, 1525.3 / 0.0 ms  (average mu = 0.074, current mu = 0.007) allocation failure scavenge might not succeed


<--- JS stacktrace --->

FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory
```


<details>
<summary>heap limit Allocation failed</summary>
<p>

```console
asdf@asdfs-MacBook-Pro sqlite % node asdf.js                      

<--- Last few GCs --->

[9710:0x102aa2000]   124422 ms: Mark-sweep 2048.6 (2051.8) -> 2048.3 (2052.1) MB, 1525.3 / 0.0 ms  (average mu = 0.074, current mu = 0.007) allocation failure scavenge might not succeed


<--- JS stacktrace --->

FATAL ERROR: Ineffective mark-compacts near heap limit Allocation failed - JavaScript heap out of memory
 1: 0x101018fd1 node::Abort() (.cold.1) [/usr/local/bin/node]
 2: 0x10008632b node::FatalError(char const*, char const*) [/usr/local/bin/node]
 3: 0x10008646c node::OnFatalError(char const*, char const*) [/usr/local/bin/node]
 4: 0x100187727 v8::Utils::ReportOOMFailure(v8::internal::Isolate*, char const*, bool) [/usr/local/bin/node]
 5: 0x1001876c7 v8::internal::V8::FatalProcessOutOfMemory(v8::internal::Isolate*, char const*, bool) [/usr/local/bin/node]
 6: 0x100312e75 v8::internal::Heap::FatalProcessOutOfMemory(char const*) [/usr/local/bin/node]
 7: 0x1003146ca v8::internal::Heap::RecomputeLimits(v8::internal::GarbageCollector) [/usr/local/bin/node]
 8: 0x10031114e v8::internal::Heap::PerformGarbageCollection(v8::internal::GarbageCollector, v8::GCCallbackFlags) [/usr/local/bin/node]
 9: 0x10030ef00 v8::internal::Heap::CollectGarbage(v8::internal::AllocationSpace, v8::internal::GarbageCollectionReason, v8::GCCallbackFlags) [/usr/local/bin/node]
10: 0x10031ad2a v8::internal::Heap::AllocateRawWithLightRetry(int, v8::internal::AllocationType, v8::internal::AllocationOrigin, v8::internal::AllocationAlignment) [/usr/local/bin/node]
11: 0x10031adb1 v8::internal::Heap::AllocateRawWithRetryOrFail(int, v8::internal::AllocationType, v8::internal::AllocationOrigin, v8::internal::AllocationAlignment) [/usr/local/bin/node]
12: 0x1002e7cad v8::internal::Factory::NewFixedArrayWithFiller(v8::internal::RootIndex, int, v8::internal::Object, v8::internal::AllocationType) [/usr/local/bin/node]
13: 0x100513847 v8::internal::BaseNameDictionary<v8::internal::NameDictionary, v8::internal::NameDictionaryShape>::New(v8::internal::Isolate*, int, v8::internal::AllocationType, v8::internal::MinimumCapacity) [/usr/local/bin/node]
14: 0x1004e2775 v8::internal::JSObject::MigrateToMap(v8::internal::Isolate*, v8::internal::Handle<v8::internal::JSObject>, v8::internal::Handle<v8::internal::Map>, int) [/usr/local/bin/node]
15: 0x1004fb4c4 v8::internal::LookupIterator::ApplyTransitionToDataProperty(v8::internal::Handle<v8::internal::JSReceiver>) [/usr/local/bin/node]
16: 0x100527a0e v8::internal::Object::AddDataProperty(v8::internal::LookupIterator*, v8::internal::Handle<v8::internal::Object>, v8::internal::PropertyAttributes, v8::Maybe<v8::internal::ShouldThrow>, v8::internal::StoreOrigin) [/usr/local/bin/node]
17: 0x10064933c v8::internal::Runtime::SetObjectProperty(v8::internal::Isolate*, v8::internal::Handle<v8::internal::Object>, v8::internal::Handle<v8::internal::Object>, v8::internal::Handle<v8::internal::Object>, v8::internal::StoreOrigin, v8::Maybe<v8::internal::ShouldThrow>) [/usr/local/bin/node]
18: 0x10019c756 v8::Object::Set(v8::Local<v8::Context>, v8::Local<v8::Value>, v8::Local<v8::Value>) [/usr/local/bin/node]
19: 0x10004a93d napi_set_property [/usr/local/bin/node]
20: 0x10502017c void Napi::Object::Set<Napi::Value>(Napi::Value, Napi::Value const&) [/Users/asdf/sqlite/node_modules/sqlite3/lib/binding/napi-v3-darwin-x64/node_sqlite3.node]
21: 0x10501c449 node_sqlite3::Statement::RowToJS(Napi::Env, std::__1::vector<node_sqlite3::Values::Field*, std::__1::allocator<node_sqlite3::Values::Field*> >*) [/Users/asdf/sqlite/node_modules/sqlite3/lib/binding/napi-v3-darwin-x64/node_sqlite3.node]
22: 0x10501df9f node_sqlite3::Statement::Work_AfterAll(napi_env__*, napi_status, void*) [/Users/asdf/sqlite/node_modules/sqlite3/lib/binding/napi-v3-darwin-x64/node_sqlite3.node]
23: 0x1000655fb (anonymous namespace)::uvimpl::Work::AfterThreadPoolWork(int) [/usr/local/bin/node]
24: 0x1008dafdc uv__work_done [/usr/local/bin/node]
25: 0x1008de559 uv__async_io [/usr/local/bin/node]
26: 0x1008ede0e uv__io_poll [/usr/local/bin/node]
27: 0x1008de991 uv_run [/usr/local/bin/node]
28: 0x1000bafc6 node::NodeMainInstance::Run() [/usr/local/bin/node]
29: 0x100062bfb node::Start(int, char**) [/usr/local/bin/node]
30: 0x7fff6cd2fcc9 start [/usr/lib/system/libdyld.dylib]
zsh: abort      node asdf.js
asdf@asdfs-MacBook-Pro sqlite % 
```
</p>
</details> 

## build a continuous stream of data

In order not to run the risk of a memory overflow, smaller data blocks are queried. 
To do this, we scroll through the *`Fires`* table in 10k blocks.


### initial query

Initial query with parameter transfer. The maximum fetch size is 10k - the maximum transfer size for elastic.

```sql
select * from Fires where objectid between 'lower limit' and 'upper limit';
```




## run the node-js program


```console
{
  count: 1880000,
  _shards: { total: 1, successful: 1, skipped: 0, failed: 0 }
}
timestamp in milliseconds: 209571
NaN
asdf@asdfs-MacBook-Pro us-wildfires % 
```




# Create index pattern

Or just look in the elastic documentation.
[index-patterns](https://www.elastic.co/guide/en/kibana/current/index-patterns.html)


1. Open the main menu, then click to *`Stack Management`* > *`Index Patterns`*.
2. Click Create index pattern.


```mermaid
graph LR
    id1(management)-->id2(indexPatterns)-->id3(Create index pattern)
    style id1 fill:#f9f,stroke:#333,stroke-width:4px
    style id2 fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
    style id3 fill:#f9,stroke:#333,stroke-width:2px
```


Time field: 'discovery_date'



## check whether all fields are mapped correctly


GET > *`http://localhost:9200/wildfires/_mapping?`*


<details>
<summary>http://localhost:9200/wildfires/_mapping?</summary>
<p>

```json
{
    "wildfires": {
        "mappings": {
            "properties": {
                "discovery_date": {
                    "type": "date"
                },
                "fire_code": {
                    "type": "keyword"
                },
                "fire_name": {
                    "type": "text"
                },
                "fire_size": {
                    "type": "double"
                },
                "fire_size_keyword": {
                    "type": "keyword"
                },
                "fod_id": {
                    "type": "long"
                },
                "fpa_id": {
                    "type": "text"
                },
                "geographic_coordinates": {
                    "type": "geo_point"
                },
                "local_incident_id": {
                    "type": "text"
                },
                "nwcg_reporting_unit_id": {
                    "type": "text"
                },
                "nwcg_reporting_unit_name": {
                    "type": "keyword"
                },
                "objectid": {
                    "type": "long"
                },
                "source_system": {
                    "type": "keyword"
                },
                "stat_cause_descr": {
                    "type": "keyword"
                }
            }
        }
    }
}
```
</p>
</details> 


# Analysis of the data

## Find the first entry

In order to find the first *`discovery_date`*, a query is needed for the most recent date.


```json
GET wildfires/_search?&size=0
{
  "aggs": {
    "mdd": {
      "min": {
        "field": "discovery_date"
      }
    }
  }
}
```

the first *`discovery_date`* is returned.

<details>
<summary>"value_as_string" : "1992-01-01T00:00:00.000Z"</summary>
<p>

```json
{
  "took" : 1,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 10000,
      "relation" : "gte"
    },
    "max_score" : null,
    "hits" : [ ]
  },
  "aggregations" : {
    "mdd" : {
      "value" : 6.94224E11,
      "value_as_string" : "1992-01-01T00:00:00.000Z"
    }
  }
}
```
</p>
</details>


*`1992-01-01`* will be the start date for further analysis.


Similarly, the query can also be carried out with the maximum date.




## Time series overview




![Visualize_Library_-_Elastic.jpg](Screenshots/Visualize_Library_-_Elastic.jpg)

With Timelion and the special expression syntax, time series can be analyzed very quickly in no time at all.

#### timelion_expressions

<details>
<summary>timelion_expressions</summary>
<p>

```console
.es(index=wildfires,timefield=discovery_date, metric=count).color(color=blue).label('number of fires per month'),
.es(index=wildfires, timefield=discovery_date, metric=avg:count).color(color=red)
```
</p>
</details> 



## More Kibana examples

Once the data to be analyzed has been indexed, it is super easy to evaluate it graphically. With Kibana, dashboards with fliter options can be created in no time at all.


### Overview with filter settings

<details>
<summary>wildfires_-_Elastic.jpg</summary>
<p>
![wildfires_-_Elastic.jpg](Screenshots/wildfires_-_Elastic.jpg)
</p>
</details>


### Maps and correlation matrix

<details>
<summary>wildfires_-_Elastic2.jpg</summary>
<p>
![wildfires_-_Elastic2.jpg](Screenshots/wildfires_-_Elastic2.jpg)
</p>
</details> 


### Function graphs and tables

<details>
<summary>wildfires_-_Elastic4.jpg</summary>
<p>
![wildfires_-_Elastic4.jpg](Screenshots/wildfires_-_Elastic4.jpg)
</p>
</details>


As shown, elastic and node-js can be used to move and transform data from a to b relatively quickly. Time series can be analyzed very quickly in Kibana.